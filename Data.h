#pragma once
#include <string>
#include <fstream>
#include <iostream>
class Data
{
public:
	Data();
	~Data();

	void load(std::string filpath);
	void parse(std::string text, double value);

	void print();


public:
	int ne; //liczba elementow
	int nh; //liczba wezlow
	double L; //dlugosc preta
	double k; // wsp 
	double alfa; //wsp 
	double q; // cieplo 
	double t0; // 
	double tpow;
	double rMax;
	double deltaR;
	double deltaTau;
	double c;
	double ro;
	double tauMax;

};

