#include "Grid.h"



Grid::Grid()
{
}
Grid::Grid(Data * data)
{
	this->data = data;
}

Grid::~Grid()
{
}

void Grid::initialGrid()
{
	//
	std::fstream plik("wynik.txt", std::ios::out | std::ios::trunc);
	if (plik.good() == true) {
		plik << "MES NIESTACJONARNY PROCES WYMIANY CIEP�A 2016 " << std::endl;
		plik.close();
	}
	this->elements = new Element[data->ne];
	this->nodes = new Node[data->nh];

	for (int i = 0; i < data->ne; i++) {
		elements[i].setId(i + 1);

		elements[i].setNOP1(&nodes[i]);
		elements[i].setNOP2(&nodes[i + 1]);
		elements[i].getId() == data->ne ? elements[i].setFlag(true) : elements[i].setFlag(false);
		elements[i].setData(data);
		elements[i].init();
	}
	for (int i = 0; i < data->nh; i++) {
		nodes[i].setTemp(data->t0);
		nodes[i].setR(i*data->deltaR);
	}

	

}

void Grid::initGlobals()
{
	Kglobal = new double*[data->nh];
	for (int i = 0; i < data->nh; i++) {
		Kglobal[i] = new double[data->nh + 1]; // +1 dla wektora F do gausa
	}
	Fglobal = new double[data->nh];
	for (int i = 0; i < data->nh; i++) {
		Fglobal[i] = 0.0;
		for (int j = 0; j < data->nh + 1; j++) {
			Kglobal[i][j] = 0.0;
		}
	}
}

void Grid::calculateLocals()
{
	for (int i = 0; i < data->ne; i++) {
		elements[i].calculateLocal();
	}
}

void Grid::calculateGlobal()
{
	initGlobals();
	for (int i = 0; i < data->ne; i++) {
		Kglobal[0 + i][0 + i] += elements[i].Klocal[0][0];
		Kglobal[0 + i][1 + i] += elements[i].Klocal[0][1];
		Kglobal[1 + i][0 + i] += elements[i].Klocal[1][0];
		Kglobal[1 + i][1 + i] += elements[i].Klocal[1][1];

		Fglobal[0 + i] += elements[i].Flocal[0] * -1;
		Fglobal[1 + i] += elements[i].Flocal[1] * -1;
		
	}
	//printGlobal();

}

void Grid::solveTime()
{
	double a = data->k / (data->c * data->ro);
	//std::cout << a << std::endl;
	//std::cout << a << data->k << std::endl << data->c << std::endl << data->ro << std::endl;
	data->deltaTau= (data->deltaR * data->deltaR) /( 0.5 * a);
	
	double TauMax = data->tauMax;
	double nTau = (TauMax / data->deltaTau) + 1;
	std::cout << nTau << std::endl;
	
	data->deltaTau = TauMax / nTau;
	int i = 0;
	//for (int i = 1; i <= nTau; i++) {
	for (int i = 0; i < data->nh; i++)
		std::cout << nodes[i].getTemp() << " ";
	while (nodes[0].getTemp() > 22) {
		for (int i = 0; i < data->nh; i++)
			std::cout << nodes[i].getTemp() << " ";
		calculateLocals();
		calculateGlobal();
		solve();
		saveTemp(i*data->deltaTau);
		//for (int i = 0; i < data->nh; i++) {
		//	std::cout << " Temp  " << nodes[i].getTemp();
		//}
		i++;
		std::cout << std::endl;
	}
	std::cout << "CZAS CHLODZENIA KOMINA WYNOSI " << i* data->deltaTau / 60 / 60 << "h";
	//}

}


void Grid::solve()
{

	for (int i = 0; i < data->nh; i++)
	{
		Kglobal[i][data->nh] = -1 * Fglobal[i];
	}
	//gausik
	for (int i = 0; i <  data->nh; i++)
	{
		for (int k = i + 1; k < data->nh; k++)
			if (Kglobal[i][i] < Kglobal[k][i])
			{
				for (int j = 0; j <= data->nh; j++)
				{			
					std::swap(Kglobal[i][j], Kglobal[k][j]);
				}
			}
	}

	for (int i = 0; i < data->nh; i++)
	{
		for (int k = i + 1; k < data->nh; k++)
		{
			double t = Kglobal[k][i] / Kglobal[i][i];
			for (int j = 0; j <= data->nh; j++)
			{
				Kglobal[k][j] = Kglobal[k][j] - t*Kglobal[i][j];
			}
		}
	}

	//zapis temp do wezlow


	double* temps = new double[data->nh];
	for (int i = 0; i < data->nh + 1; i++)
		temps[i] = 0.0;
	//--------------------
	for (int i = (data->nh - 1); i >= 0; i--)
	{
		temps[i] = Kglobal[i][data->nh];

		for (int j = 0; j <= data->nh; j++)

			if (j != i)
				temps[i] -= Kglobal[i][j] * temps[j];
		temps[i] = temps[i] / Kglobal[i][i];
	}
	//zapisz temp wezlowych do wez��w
	for (int i = 0; i < data->nh; i++)
	{
		nodes[i].setTemp(temps[i]);
	}

}

void Grid::saveTemp(int krok)
{

	std::fstream plik("wynik.txt", std::ios::app);
	if (plik.good() == true)
		{
			plik << std::endl<< krok<< ": ";
			for (int i = 0; i < data->nh; i++)
				plik << nodes[i].getTemp() << " ";


			plik.close();
		}


	
}


void Grid::clean()
{
	for (int i = 0; i < data->ne; i++) {
		elements[i].clean();
	}
	delete[] elements;
	delete[] nodes;

	for (int i = 0; i < data->nh; i++) {
		delete[] Kglobal[i];
	}
	delete[] Kglobal;
	delete[] Fglobal;
}

void Grid::printGlobal()
{
	for (int i = 0; i < data->nh; i++) {
		std::cout << "F[" << i << "]" << Fglobal[i] << " "<< std::endl;
		for (int j = 0; j < data->nh + 1; j++) {
			std::cout << "H[" << i <<","<<j<< "]" << Kglobal[i][j] << " ";
		}
		std::cout << std::endl;
	}
}
