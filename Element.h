#pragma once
#include "Node.h"
#include "Data.h"
class Element
{
private:
	int id;
	bool flag;
	Node * NOP1;
	Node * NOP2;
	

	Data * data;

public:
	Element();
	~Element();
	void clean();
	
	void setNOP1(Node* node);
	void setNOP2(Node * node);

	//flaga ktory element ma warunek brzegowy
	void setFlag(bool flag);
	bool getFlag();
	int getId();

	void setId(int id);

	void setData(Data *data);

	void init();

	void printLocals();
	void calculateLocal();

	double ** Klocal;
	double * Flocal;

};

