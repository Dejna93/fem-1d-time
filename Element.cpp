#include "Element.h"

#include <iostream>

Element::Element()
{
}


Element::~Element()
{
}

void Element::init()
{
	Flocal = new double[2];
	Klocal = new double*[2];
	for (int i = 0; i < 2; i++) {
		Klocal[i] = new double[2];
	}
	for (int i = 0; i < 2; i++) {
		Flocal[i] = 0.0;
		for (int j = 0; j < 2; j++) {
			Klocal[i][j] = 0.0;
		}
	}
}


void Element::printLocals()
{
	std::cout << " K[0][0] " << Klocal[0][0] << " K[0][1] " << Klocal[0][1]
		<< "\n K[1][0] " << Klocal[1][0] << " K[1][1] " << Klocal[1][1] << std::endl << std::endl;

	std::cout << "F[0] " << Flocal[0] << " F[1] " << Flocal[1] <<std::endl;
}

void Element::calculateLocal()
{
	for (int i = 0; i < 2; i++) /*Zerowanie*/
		for (int j = 0; j < 2; j++)
		{
			Klocal[i][j] = 0.0;
		}
	Flocal[0] = 0.0; Flocal[1] = 0.0; 
	double dTau = data->deltaTau;
	double E[2] = { -0.5773502692, 0.5773502692 };
	//double E[2] = { -0.5773, 0.5773 };
	double N1[2] = { 0.5*(1 - E[0]), 0.5*(1 - E[1]) };
	double N2[2] = { 0.5*(1 + E[0]), 0.5*(1 + E[1]) };
	double rp=0;
	double temptau = 0;
	double w = 1;



	for (int i = 0; i < 2; i++) {

		rp = (N1[i] * NOP1->getR()  + N2[i] * NOP2->getR());
		temptau = (N1[i] * NOP1->getTemp() + N2[i] * NOP2->getTemp());

		Klocal[0][0] += data->k*rp*w / data->deltaR + data->c*data->ro*data->deltaR*rp*w*N1[i] * N1[i] / dTau;
		std::cout << (data->k * rp * w) / data->deltaR << " " << (data->c * data->ro * rp * w * N1[i] * N1[i]) / dTau << std::endl;
		Klocal[0][1] += ((-1) * data->k * rp * w) / data->deltaR + (data->c * data->ro * data->deltaR * rp * w * N1[i] * N2[i]) / dTau;

		Klocal[1][0] = Klocal[0][1];
		Klocal[1][1] += data->k*rp*w / data->deltaR + data->c*data->ro*data->deltaR*rp*w*N2[i] * N2[i] / dTau;
		
		Flocal[0] += (data->c * data->ro * data->deltaR * temptau * rp * w * N1[i]) / dTau;

		Flocal[1] += (data->c * data->ro * data->deltaR * temptau * rp * w * N2[i]) / dTau;

	}
	if (flag == true) {
		Flocal[1] += 2 * data->alfa*data->rMax * data->tpow;
		Klocal[1][1] += 2 * data->alfa*data->rMax;
	}

//	printLocals();
}


void Element::setNOP1(Node * node)
{
	this->NOP1 = node;
}

void Element::setNOP2(Node * node)
{
	this->NOP2 = node;
}

void Element::setFlag(bool flag)
{
	this->flag = flag;
}

bool Element::getFlag()
{
	return flag;
}

int Element::getId()
{
	return id;
}

void Element::setId(int id)
{
	this->id = id;
}

void Element::setData(Data * data)
{
	this->data = data;
}

void Element::clean() {
	//wskazniki ...
	for (int i = 0; i < 2; i++) {
		delete[]Klocal[i];
	}
		delete[]Klocal;
		delete[]Flocal;
	
}
