#include <iostream>
#include <string>
#include "Data.h"
#include "Grid.h"
#include <time.h>
using namespace std;

int main() {

	clock_t begin = clock();
	Data *data = new Data();
	data->load("data.txt");
	Grid *grid = new Grid(data);
	

	grid->initialGrid();
	grid->solveTime();

	//grid->clean();
	clock_t end = clock();

	cout << "Czas oblicznien " << double(end - begin) / CLOCKS_PER_SEC;
	system("pause");

}