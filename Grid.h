#pragma once
#include "Data.h"
#include "Element.h"
#include "Node.h"
class Grid
{
private:
	Element * elements;
	Node * nodes;

	double ** Kglobal;
	double * Fglobal;

	Data * data;

public:
	Grid();
	Grid(Data *data);
	~Grid();

	void initialGrid();
	void initGlobals();
	void calculateLocals();
	void calculateGlobal();
	void solveTime();
	void solve();
	void saveTemp(int krok);
	void clean();


	void printGlobal();
};

