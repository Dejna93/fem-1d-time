#include "Data.h"



Data::Data()
{
}


Data::~Data()
{
}


void Data::load(std::string filepath) {
	std::fstream plik;
	plik.open("dane.txt", std::ios::in);

	if (plik.good()) {
		std::string text;
		double value;
		while (!plik.eof()) {
			plik >> text >> value;
			parse(text, value);
		}
		plik.close();

	}

}

void Data::parse(std::string text, double value) {
	if (text == "ne") {
		this->ne = value;
		this->nh = ne + 1; 
	}
	if (text == "L") {
		this->L = value;
	}
	if (text == "k") {
		this->k = value;
	}
	if (text == "q") {
		this->q = value;
	}
	if (text == "alfa") {
		this->alfa = value;
	}
	if (text == "rMax") {
		this->rMax = value;
	}
	if (text == "deltaR") {
		this->deltaR = value;
	}
	if (text == "deltaTau") {
		this->deltaTau = value;
	}
	if (text == "c") {
		this->c = value;
	}
	if (text == "ro") {
		this->ro = value;
	}
	if (text == "t0") {
		this->t0 = value;
	}
	if (text == "tpow") {
		this->tpow = value;
	}
	if (text == "tauMax") {
		this->tauMax = value;
	}
}

void Data::print() {
	std::cout <<" ne " <<  ne << " nh " << nh  << " L " <<
		L << " k " << k << " alfa " << alfa << " q " << q	<< " t0 " << t0	
		<< " tpow " << tpow
		<< " rmax " << rMax
		<< " dr " << deltaR
		<< " dtau " << deltaTau
		<< " c " << c
		<< " ro " << ro
		<< " taumax " << tauMax;
}