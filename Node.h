#pragma once
class Node
{
private:
	int id;
	double t;
	double r;

public:
	Node();
	Node(int id);
	~Node();

	void setId(int id);
	void setTemp(double t);
	void setR(double r);

	double getTemp();
	double getR();
	int getId();




};

